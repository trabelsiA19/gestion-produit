import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProduitService {
  public url=environment.apiUrl+"rest/api/";

  constructor(private http:HttpClient) { }

  allProduit(token:any){
    var headers_object = new HttpHeaders().set("Authorization",token);
    const httpOptions ={
      headers: headers_object
    };
    return this.http.get<any>(this.url+"allProduit",httpOptions);
  }

  getProduit(id:number,token:any){
    var headers_object = new HttpHeaders().set("Authorization",token);
    const httpOptions ={
      headers: headers_object
    };
    return this.http.get<any>(this.url+"produit/"+id,httpOptions);
  }

  addProduit(produit:Object,token:any){
    var headers_object = new HttpHeaders().set("Authorization",token);
    const httpOptions ={
      headers: headers_object
    };
    return this.http.post<any>(this.url+"addProduit",produit,httpOptions);
  }

  updateProduit(id:number,produit:Object,token:any){
    var headers_object = new HttpHeaders().set("Authorization",token);
    const httpOptions ={
      headers: headers_object
    };
    return this.http.put<any>(this.url+"produit/"+id,produit,httpOptions);
  }

  deleteProduit(id:number,token:any){
    var headers_object = new HttpHeaders().set("Authorization",token);
    const httpOptions ={
      headers: headers_object
    };
    return this.http.delete<any>(this.url+"produit/"+id,httpOptions);
  }
}
