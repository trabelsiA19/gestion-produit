package com.itbs.projet.model;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}