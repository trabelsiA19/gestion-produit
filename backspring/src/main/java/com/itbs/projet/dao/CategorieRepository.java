package com.itbs.projet.dao;

import com.itbs.projet.model.Categorie;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("com.itbs.projet.dao")
public interface CategorieRepository extends JpaRepository<Categorie,Integer> {
}
