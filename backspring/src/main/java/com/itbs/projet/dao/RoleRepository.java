package com.itbs.projet.dao;

import java.util.Optional;

import com.itbs.projet.model.Role;
import com.itbs.projet.model.RoleName;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
 
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}